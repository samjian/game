<?php

namespace App\Admin\Action;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;

class LinkButton extends AbstractTool
{
    public $name;
    public  $href;
    public function __construct($name, $href){
        $this->name = $name;
        $this->href =$href;
    }

    public function render()
    {
        $url =$this->href;
        $text =  $this->name;
        return <<<HTML
            <a href="$url"    class="btn btn-sm btn-warning">$text</a>
            HTML;
    }
}