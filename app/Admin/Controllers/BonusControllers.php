<?php

namespace App\Admin\Controllers;

use App\Models\Bonus;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Admin\Util\ActionButton;
use App\Models\LotteryList;
use App\Models\BonusSettings;

class BonusControllers extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans("獎品設定"))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('admin.edit'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('admin.create'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Bonus);
        $game_id = request()->g;
        /*
        $grid->actions(function (Grid\Displayers\DropdownActions $actions) { 
            $actions->add(new CustomView());
        });*/
        
        $grid->actions(function (  $actions) {
            $actions->disableEdit();
            $actions->disableView();
            
            $id = $actions->row->id;
            $g = request()->g;
            
            $actions->add(new ActionButton("編輯", url("/admin/bonus/$id/edit?g=$g")));
        });
        
          
        
        $grid->model()->where('game_id', $game_id);
        
        $grid->column('game.name' , "遊戲名稱"); 
        $grid->name('獎項名稱');
        $grid->img('獎項圖片')->image();
        $grid->qty('數量');
        
        $grid->column('剩餘數量')->display(function($id) {
             $count =  LotteryList::where("bonus_id",$this->id)->where("is_success",1)->count();
             return $this->qty - $count;
        });
        /*    
        $grid->column('未抽獎數')->display(function($id) {
            $max_num =  LotteryList::where("bonus_id",$this->id)->max("num");
            
            if (!$max_num) $max_num =0;
            
            $count = BonusSettings::where("bonus_id", $this->id)->where("no" ,">=" ,$max_num )->count();
            return "$count";
        });
        */
        $grid->column('尚可設定抽獎數')->display(function($id) {
            $lotteryListcount =  LotteryList::where("bonus_id",$this->id)->where("is_success",1)->count();
            $count  = $this->qty - $lotteryListcount; //剩餘數量
             
            $max_num =  LotteryList::where("bonus_id",$this->id)->max("num");
            
            if (!$max_num) $max_num =0;
            
            $bonusSettingcount = BonusSettings::where("bonus_id", $this->id)->where("no" ,">=" ,$max_num )->count(); //未抽序號數
            return $count - $bonusSettingcount;
        });
        
        $grid->column('中獎設定')->display(function() {
            return "<a href='".url("/admin/bonusSetting/create?bs=$this->id&g=$this->game_id")."'>設定</a>";
        });
        
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Bonus::findOrFail($id));

       
        $show->name('獎項名稱');
        $show->img('獎項圖片')->image();
        $show->qty('數量');
      
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Bonus);
       
        $game_id = request()->g;
      
        $form->hidden('game_id')->value($game_id);
         
        $form->text('name', '獎項名稱')->required();
        $form->image('img', '獎項圖片')->required();
        $form->number('qty', '數量')->value(1)->min(1);
    
        $form->saved(function (Form $form) {
            return redirect(url('admin/bonus?g='.$form->game_id));
        });
        return $form;
    }
}
