<?php

namespace App\Admin\Controllers;

use App\Models\BonusSettings;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Models\Bonus;
use App\Admin\Util\ActionButton;
use App\Models\Game;
use App\Models\LotteryList;
use App\Admin\Action\LinkButton;

class BonusSettingController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header("中獎設定")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('admin.edit'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('admin.create'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
 
    {
        $bs = request()->bs;
        $grid = new Grid(new BonusSettings);
        $grid->model()->where('bonus_id', $bs);
        $grid->model()->orderBy('no', 'desc');
        $bs = request()->bs;
        
        //加入匯出功能按鈕
        $grid->tools(function (Grid\Tools $tools) {
            $g = request()->g;
        
            $tools->append(new LinkButton("返回獎品設定",url("admin/bonus?g=$g")));
          
        });
        
        
        
        $grid->actions(function (  $actions) {
            $actions->disableEdit();
            $actions->disableView();
            
            $id = $actions->row->id;
            $bs = request()->bs;
            $g = request()->g;
            
           
               
        });
        
    
         
        
        $grid->column('game.name' , "遊戲名稱");   
        $grid->column('bonus.name' , "獎項");   
        $grid->no('中獎人序號');
        
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BonusSettings::findOrFail($id));

        $show->id('ID');
        $show->column('bonus.name');
        $show->no('no');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BonusSettings);

       
        $bs_id = request()->bs;
        $game_id = request()->g;
        
        $bonus =  Bonus::find($bs_id);
       
        $num_min = BonusSettings::where("bonus_id",$bs_id)->max("no");
        
        $num_min++;
        
        //$game = Game::find($game_id);
        
        
        $form->hidden('num_min')->value($num_min);
        $form->hidden('bs_id')->value($bs_id);
        $form->hidden('game_id')->value($game_id);
        $form->hidden('qty')->value(0);
        if ($bonus) {
            $form->display('獎項名稱')->value($bonus->name)->disable();;
            $lotteryListcount =  LotteryList::where("bonus_id",$bs_id)->where("is_success",1)->count();
            $count  = $bonus->qty - $lotteryListcount; //剩餘數量
            
            
            
            $max_num =  LotteryList::where("bonus_id",$bs_id)->max("num");
            
            if (!$max_num) $max_num =0;
            
            $bonusSettingcount = BonusSettings::where("bonus_id", $bs_id)->where("no" ,">=" ,$max_num )->count(); //未抽序號數
            
            $form->display('尚可設定抽獎數')->value($count-$bonusSettingcount);
            
            $form->hidden('qty')->value($count-$bonusSettingcount);
        }
        
        
        
        $form->number('num', '中獎人數量')->value(1)->min(1);
        $form->number('num_max', '當前中獎序號起始號')->value($num_min)->min($num_min);
        
        
        $form->html("
          1、每次設定中獎人數量需小於等於尚可設定抽獎數  <br>
          2、如要增加尚可設定抽獎數，請先調整獎品數量 <br>
          3、中獎序號可隨意填寫大於中獎人數量的值<br>
          4、例:中獎人數2，序號 100號，表示100號隨機，抽中兩號，例如 10、21號
        ","操作說明");
        
      
        
      
        
        $form->footer(function ($footer) {
            
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
        });
        
         $form->saving(function (Form $form) {
             
            if ($form->qty < 0 || $form->qty < $form->num) {
                 admin_toastr("尚可設定抽獎數不足，請先增加獎項數量","error");
                 return back();
            }
            
            $bs_id = $form->bs_id;
            $game_id = $form->game_id;
            
            $num = $form->num;
            $num_min = $form->num_min;
            $num_max = $form->num_max;
            
            $randArr = array();
            
            $arr = range($num_min, $num_max);
            
            shuffle($arr);
            
            if ($num > count($arr)) {
                $num = count($arr);
            }
            
            //產生序號
            for($i=0; $i< $num ;$i++) {
                $new_bs = new BonusSettings;
                $new_bs->bonus_id = $bs_id;
                $new_bs->game_id = $game_id;
                $new_bs->no = $arr[$i];
                $new_bs->save();
            }
           
            
            return redirect(url("/admin/bonusSetting?bs=$form->bs_id&g=$form->game_id"));
        });
        
             
        return $form;
    }
}
