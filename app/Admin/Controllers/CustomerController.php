<?php

namespace App\Admin\Controllers;

use App\Models\Customer;
use App\Models\Game;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header("帳戶管理")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('admin.edit'))
            ->description(trans('admin.description'))
            ->body($this->form($id)->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('admin.create'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Customer);

       
        $grid->name(  "客戶名稱");
        $grid->username("帳號");
         
        $grid->column('status', '狀態')->using([
            0 => '停用',
            1 => '啟用',
        ])->dot([
            0 => 'danger',
            1 => 'success',
        ]);
        
        $grid->column('門市管理')->display(function($id) {
            return "<a href='".url("/admin/store?c=")."$this->id'>進入</a>";
        });
        
        $grid->column('遊戲管理')->display(function($id) {
            return "<a href='".url("/admin/game?c=")."$this->id'>進入</a>";
        });
         
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableDelete();
        });
        
        return $grid;
    }
     
    
    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Customer::findOrFail($id));

        
        $show->name('客戶名稱');
        $show->username('帳號');
        $show->email('信箱');
        $show->status('狀態')->as(function ($status) {
            return $status == 1 ? "啟用" : "停用";
        });
        
        
        
        
        //$show->field('test', "games.status");
        $show->games("可用遊戲")->as(function ($games) {
            $str ="";
            foreach ($games as $game) {
                $str.= $game->name ."&nbsp;&nbsp;&nbsp;";
            }
            return $str;
        });
        
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }
    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id =null)
    {
        
        $permissionModel = config('admin.database.permissions_model');
        $roleModel = config('admin.database.roles_model');
        
        $form = new Form(new Customer);
        $form->ignore(['games']);
        
        
        $form->hidden('id');
        $form->text('name', '客戶名稱')->required();
        $form->text('username', '帳號')->required() ->creationRules(['required', "unique:customers"]);
        $form->password('password', '密碼')->required();
        
        $form->multipleSelect('roles', trans('admin.roles'))->options($roleModel::where("name" ,'<>' ,'Administrator')->pluck('name', 'id'));
        $form->multipleSelect('permissions', trans('admin.permissions'))->options($permissionModel::all()->pluck('name', 'id'));
        
        $form->email('email', 'Email')->required();
        
        $form->switch('status', '狀態')->states( [
            'on'  => ['value' => 1, 'text' => '啟用' ],
            'off' => ['value' => 0, 'text' => '不啟用' ],
        ])->default(1);
        
        $checked = [];
        $form ->updateId = $id;
        if ($id != null) {
            $games  =  Game::where("customer_id", $id)->get();
            foreach ($games as $game) {
                if ($game ->status ==1) {
                    $checked [] = $game->type;
                }
            }
        } else {
            foreach (Game::types as $game) {
                $checked [] = $game["id"];
            }
        }
         
        $form->checkbox("games","可用遊戲")
               ->options(Game::typeArray())
               ->checked($checked)
              ->rules('required',["required"=>"請選擇一種遊戲"])
        ; 
         
              
       
        $form->saving(function (Form $form) {
           
          if ($form->password && $form->model()->password != $form->password) {
              $form->password = Hash::make($form->password);
          }
        });
      
                     
        $form->saved(function (Form $form) {
            $gameids  = request()->games;
            $id  =  $form ->id;
            
            $games = null;
            
            if ($id ==null) {
                $games = Game::types;
                foreach (Game::types as $type){
                    
                    $game =  new Game;
                    
                    if ( in_array($type["id"], $gameids)) {
                        $game->status = 1;
                    } else {
                        $game->status = 0;
                    }
                    
                    $game->type = $type["id"];
                    $game->name = $type["name"];
                    $game->customer_id = $form->model()->id;
                    $game->save();
                }   
            } else {
                foreach (Game::types as $type){
                    $isExists= Game::where("customer_id", $id)->where("type", $type["id"])->exists();
                    //不存在新增遊戲
                    if ($isExists == false) {
                        $game =  new Game;
                        $game->type = $type["id"];
                        $game->name = $type["name"];
                        $game->customer_id =$id;
                        $game->save();
                    }
                }
                
                $customer = Customer::find($id);
                $games = $customer->games;
               
                foreach ($games as $game){
                   
                    if ( in_array($game->type, $gameids)) {
                        $game->status =1;
                    } else {
                        $game->status =0;
                    }
                    $game->save();
                } 
                
                
            }
        });

        return $form;
    }
    
    
    
}
