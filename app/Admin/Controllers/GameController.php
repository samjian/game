<?php

namespace App\Admin\Controllers;

use App\Models\Game;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\Customer;

use Illuminate\Http\Request;



class GameController extends Controller
{
    use HasResourceActions;
    use DefaultDatetimeFormat;
    
    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header("遊戲管理")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('admin.edit'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('admin.create'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        
        $grid = new Grid(new Game);
        $grid->disableCreateButton();
        $grid->disableActions();
        $grid->disableRowSelector();
        $grid->disableExport();
        
        $id = request()->c;
         
        if ($id != null){
            $grid->model()->where('customer_id', $id);
        } else if (Customer::isUser(auth()->user())) {
            $grid->model()->where('customer_id', auth()->user()->id);
        } 
        //一次只有一個檔期
        $grid->model()->where("type", Game::types[0]["id"]);
    
        $grid->column('customer.name', '客戶名稱');
        
        $grid->name('遊戲名稱');
         
        $grid->column('status', '狀態')->display(function ($status) {
            return $status ? '啟用' : '停用';
        });
        
        $grid->column("id",'獎品設定')->display(function($id) {
            return "<a href='".url("/admin/bonus?g=$id")."'>設定</a>";
        });
        
       
        
        $grid->column('得獎名單')->display(function($id) {
            return "<a href='".url("/export/lottery/")."/$this->id' target='_blank'>下載</a>";
        });
       
       
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Game::findOrFail($id));

        $show->id('ID');
        $show->name('name');
        $show->status('status');
        $show->bonus_id('bonus_id');
        $show->bonus_setting_id('bonus_setting_id');
        $show->bonus_list_id('bonus_list_id');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Game);

        $form->display('ID');
        $form->text('name', 'name');
        $form->text('status', 'status');
        $form->text('bonus_id', 'bonus_id');
        $form->text('bonus_setting_id', 'bonus_setting_id');
        $form->text('bonus_list_id', 'bonus_list_id');
        $form->display(trans('admin.created_at'));
        $form->display(trans('admin.updated_at'));

        return $form;
    }
    
   
}
