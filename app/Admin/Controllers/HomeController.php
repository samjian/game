<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;

class HomeController extends Controller
{ 
    
    public function index(Content $content)
    {
        if (auth()->user()->isAdministrator() || auth()->user()->username=="admin") {
            return redirect("admin/customer");
        } else {
            return redirect("admin/store");
        }
    }
}
