<?php

namespace App\Admin\Controllers;

use App\Models\Store;
use App\Models\Customer;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Admin;
use App\Models\Game;

class StoreController extends Controller
{
    use HasResourceActions;
    
    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        
        return $content
            ->header("門市管理")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('admin.edit'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
         
        return $content
            ->header(trans('admin.create'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $id = request()->c;
      
        $grid = new Grid(new Store);
        
        if ($id != null){
            $grid->model()->where('customer_id', $id);
        } else if (Customer::isUser(auth()->user())) {
            $grid->model()->where('customer_id', auth()->user()->id);
        } 
         
       
        
        $grid->column('customer.name', '客戶名稱');
        $grid->name('門市名稱');
        $grid->store_no('代號');
        
        $grid->column('status', '狀態')->using([
            0 => '停用',
            1 => '啟用',
        ])->dot([
            0 => 'danger',
            1 => 'success',
        ]);
        
        
        $grid->column('戳戳樂 URL')->display(function() {
            
            $game = Game::where("customer_id", $this->customer_id)->where("type", Game::types[0]["id"])->first();
            $url = url("/play/")."/$this->id/$game->id";
            return $url;
        }); 
        
        /*
        $grid->column('拉霸 URL')->display(function($id) {
            $game = Game::where("customer_id", $this->customer_id)->where("type", Game::types[1]["id"])->first();
            $url = url("/play/")."/$this->id/$game->id";
            return "<a href='$url'>開啟</a>";
        });*/ 
        
        
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));
        
        return $grid;
        
        
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Store::findOrFail($id));

     
      
        $show->name('門市名稱');
        $show->store_no('代碼');
        $show->status('狀態')->as(function ($status) {
            return $status == 1 ? "啟用" : "停用";
        });
       
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Store);
        
        $customer_id = request()->c;
        
        if (!$customer_id) {
            $customer_id = auth()->user()->id;
        }
        
        
        $form->hidden('customer_id')->value($customer_id);
       
       
        $form->text('name', '門市名稱')->required();
        $form->text('store_no', '代碼')->required();
        
        $form->switch('status', '狀態')->states( [
            'on'  => ['value' => 1, 'text' => '啟用' ],
            'off' => ['value' => 0, 'text' => '不啟用' ],
        ])->default(1);
        
        $form->saved(function (Form $form) {
            return redirect(url('admin/store?c='.$form->customer_id));
        });
        
        return $form;
    }
}
