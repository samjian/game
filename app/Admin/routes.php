<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    
    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('/customer', 'CustomerController');
    $router->resource('/game', 'GameController');
    $router->resource('/store', 'StoreController');
    $router->resource('/bonus', 'BonusControllers');
    $router->resource('/bonusSetting', 'BonusSettingController');
    
 
});
