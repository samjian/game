<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use App\Models\LotteryList;
use Maatwebsite\Excel\Concerns\WithColumnWidths;


class LotteryListExport implements FromArray , WithColumnWidths
{
    public $gameId;
    public function __construct($gameId){
        $this->gameId= $gameId;
    }
     
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $titles =["品項","得獎姓名" ,"電話"];
        $array [] = $titles;
        
      
        
        $lotteryList =  LotteryList::where("game_id", $this->gameId)
                                    ->where("is_success", 1)
                                     ->get();
       
        foreach ($lotteryList as $lottery) {
            $user = $lottery->user()->first();
            
            if ($lottery->bonus() ==null || $user == null)   continue;
            
            $bonus = $lottery->bonus()->first();
          
            $array []=[
                $bonus->name,
                $user->name,
                $user->phone,
            ];
        }
        
         
        return $array;
    }
    /**
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\WithColumnWidths::columnWidths()
     */
    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 20,
            'C' => 30,
        ];
        
    }

    
    
   
   
}
