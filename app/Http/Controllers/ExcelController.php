<?php

namespace App\Http\Controllers;

use App\Exports\LotteryListExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
   
    public function lottery(Request $request,$gameId) {
        return Excel::download(new LotteryListExport($gameId), '得獎名單.xlsx');
    }
    
   
}
