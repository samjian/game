<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BonusSettings;
use App\Models\Game;
use App\Models\LotteryList;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Store;


class PlayController extends Controller
{
     public $gmae_result_url = "https://imagepro-engage.ml:9197/gameResult";
    
    
    /**
     * 進入遊戲  url $customer->username  + $game->type + index
     * @param Request $request
     * @param unknown $store_id
     * @param unknown $game_id
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Request $request, $store_id,$game_id) {                           
        $game = Game::find($game_id);
        
        if ($game == null) {
            abort(403, '遊戲尚未啟用');
        }
        
        if ($game->customer() == null) {
            abort(403, '遊戲尚未啟用');
        }
        
        $customer = $game->customer()->first();
        
        if ($customer->status ==0) {
            abort(403, '該廠商帳號已停權');
        }
        
        $store = Store::find($store_id);
         
        if ($store ==null || $store->status ==0) {
            abort(403, '該店家帳號已停權');
        }
        
        //檢查view是否存在
        if(!view()->exists("$customer->username.$game->type.index")){
            abort(403, '遊戲尚未啟用，請洽管理員!');
        }
        
        if ($customer->username == "kingtea") {
            $data = $request->data;
            
            if (!$data) {
                abort(403, '禁止進入遊戲，請由前端登入!');
            }
            
            $user_data = json_decode(base64_decode($data));
          
            session(['user_data'=> $user_data]);
        }
        
        return view("$customer->username.$game->type.index",[
            "store_id" => $store_id,
            "game_id" => $game_id,
        ]);
    }
    
    public function login(Request $request, $store_id,$game_id) {
        
        $user_data =  session('user_data');
        /*
        if (!$user_data) {
            abort(403, '禁止進入遊戲，請由前端登入!');
        }*/
        
        $user = User::where("line_id",  $user_data->user->lineId)->first();
        
        $game = Game::find($game_id);
        
        $customer = $game->customer()->first();
        
        //使用者不存在 導入 新增頁面
        if ($user == null) {
            return view("$customer->username.$game->type.create", [
                "store_id" => $store_id,
                "game_id" => $game_id,
                "line_id" =>$user_data->user->lineId,
            ]);
        } else {           
            if (session("user")){
                $user_session = session("user");
                if ($user_session->login_date <  date('Y-m-d')) {
                    $user->login_date =  date('Y-m-d');
                    $user->lottery_num =0;
                    session(['user'=> $user]);
                }
            } else {
                $user->login_date =  date('Y-m-d');
                $user->lottery_num =0;
                session(['user'=> $user]);
            }
            
        }
        
        return redirect("/play/toGame/$request->store_id/$request->game_id");
    }
    
   
     
    public function toGame(Request $request , $store_id,$game_id  ){
        /*
        $user_data =  session('user_data');
        
        if (!$user_data) {
            abort(403, '禁止進入遊戲，請由前端登入!');
        }*/
        
        /*
        if (!session("user")) {
            return redirect("/play/$store_id/$game_id");
        }*/
        
        $game = Game::find($game_id);
        
        $customer = $game->customer()->first();
        
        return view("$customer->username.$game->type.lottery", [
            "store_id" => $store_id,
            "game_id" => $game_id,
        ]);
    }
    
    public function create(Request $request) {
       // if (!session('user')) {
            $user= new User;
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->line_id = $request->line_id;
            
            $user->save();
            
            $user->lottery_num =0;
            $user->login_date =  date('Y-m-d');
            $user->is_success = false;
            
            session(['user'=> $user]);
       // }
       // return $this->toGame($request ,$request->store_id, $request->game_id);
        
        return redirect("/play/toGame/$request->store_id/$request->game_id");
    }
    
     
    
    public function line(Request $request) {
        $client_id = env("line_client_id");
        $client_secret = env("line_client_secret");
        $redirect_uri = env("line_redirect_uri");
        
        $arr =  $pieces = explode("_", $request->state);
        $store_id = $arr[0];
        $game_id = $arr[1];
        
        $url = 'https://api.line.me/oauth2/v2.1/token';
        $params = 'code=' . $_GET['code'] . "&client_id=$client_id&client_secret=$client_secret&grant_type=authorization_code&redirect_uri=$redirect_uri";
         
        
        $response = $this->httpRequest($url,$params);
        
        $result = json_decode($response);
        
        if (isset($result->error)) {
            echo  "<script> window.opener.postMessage('line error','*'); </script>";
            return;
        }
        
        
        $ch = curl_init("https://api.line.me/v2/profile");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Bearer ' .  $result->access_token)
            );
        $response = curl_exec($ch);
        curl_close($ch);
        
        $result = json_decode($response);
        
         
        
        
        
        //if (!isset($result->userId)) {
           return redirect("/play/login/$store_id/$game_id/$result->userId");
    }
    
   public function lottery (Request $request, $storeId, $gameId) {
       /*
       if (!session("user")) {
           return redirect("/play/$storeId/$gameId");
       }*/
       
       $user = session("user");
        
       $user->lottery_num++;
        
      
       if ( $user->lottery_num >3){
          $this->sendGameResult();
          return redirect()->back()->with("msg","遊戲一天只能玩三次!")->withinput();
       }  
       
       if ($user->is_success) {
           return redirect()->back()->with("msg","今日已經中獎!")->withinput();
       }
       
        
       $is_success = false;
       $lottery_num =0;
       $bonus_id =0;
       
       $game = Game::find($gameId);
       
       $customer = $game->customer()->first();
        
       $bonus = $game->bonus()->get();
       $bonus_temp = null;
       DB::beginTransaction();
       
       if (count($bonus) >0 ) {
           $random = rand(0, count($bonus)-1);
            
           $bonus_temp = $bonus[$random] ;
           $bonus_id = $bonus_temp->id;
           
           $successCount = DB::table('lottery_lists')
                     ->selectRaw('COUNT(1) as count')
                      ->whereRaw("game_id='$gameId' and bonus_id=$bonus_id and is_success =1")->lockForUpdate()
                     ->first()->count ;
          
          //中獎數量 需少於獎品數量
          if ($successCount < $bonus_temp->qty) {
               //抽獎編號
               $lottery_num = DB::table('lottery_lists')
                               ->selectRaw('COUNT(1) as count')
                               ->whereRaw("game_id='$gameId' and bonus_id=$bonus_id" )->lockForUpdate()
                               ->first()->count +1;
               
               //比對中獎
               if (BonusSettings::where("game_id", $gameId)->where("bonus_id",$bonus_id)->where("no", $lottery_num)->exists()) {
                   $is_success = true; //中獎
                   $user->is_success = true;
               }
           }
       } 
       
       $lotteryList = new LotteryList;
       
       $lotteryList->user_id = $user->id;
       $lotteryList->bonus_id = $bonus_id;
       $lotteryList->game_id =$gameId;
       $lotteryList->store_id = $storeId;
       $lotteryList->is_success = $is_success;
       $lotteryList->num = $lottery_num;
      
       $lotteryList->save();
       
       DB::commit();
       
     
       //有中獎
       if ($is_success) {
           
           $this->sendGameResult();
           
           return view("$customer->username.$game->type.success", [
               "store_id" => $request->store_id,
               "game_id" => $request->game_id,
               "lotteryList"=> $lotteryList,
               "user" =>$user,
               "bonus"=>$bonus_temp
           ]);
       } else { //無中獎
         
           return view("$customer->username.$game->type.fail", [
               "store_id" => $request->store_id,
               "game_id" => $request->game_id,
               "lotteryList"=> $lotteryList,
               "user" =>$user,
           ]);
       }
   }
   
   public function sendGameResult() {
       try {
           $user_data = session('user_data');
           
           if ($user_data) {
               return "";
           }
           
           $user = session('user');
           
           $resutl =  new \stdClass();
           
           //$resutl->is_success =  ($user->is_success ? "true" : "false");
           
           //只送一個true 開關
           $resutl->is_success = ture;
           
           $user_data->result =$resutl;
            
           $json_str = json_encode( $user_data);
           
           $data =  base64_encode($json_str);
           
           $url = $this->gmae_result_url ."?data=".$data;
           
           session()->forget('user_data');
           
           $str =  file_get_contents($url);
       } catch (\Exception  $e) {
           
       }
       
   }
    
   private function httpRequest($api, $data_string) {
        
        $ch = curl_init($api);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: ' . strlen($data_string))
            );
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }
    
   
}
