<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class PlayMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //DB::connection()->enableQueryLog();
  
        if (!session('user_data')) {
            abort(403, '禁止進入遊戲，請由前端登入!');
        }

        return $next($request);
    }
}

