<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bonus extends BaseModel
{
    protected $table = 'bonus';
    
    
    public function game()
    {
        return $this->belongsTo(Game::class,'game_id');
    }
    
    
    public function bonusSettings()
    {
        return $this->hasMany(BonusSettings::class,   "bonus_id");
    }
}
