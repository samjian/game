<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonusSettings extends BaseModel
{
    protected $table = 'bonus_setting';
    
    
    public function game()
    {
        return $this->belongsTo(Game::class,'game_id');
    }
    
    public function bonus()
    {
        return $this->belongsTo(Bonus::class,'bonus_id');
    }
}
