<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use   Encore\Admin\Auth\Database\Administrator;

class Customer extends Administrator  
{
    
    public static function isUser($customer) {
        return  !($customer->username == "root" || $customer->username =="admin");
    }
    
    public function games()
    {
        return $this->hasMany(Game::class,'customer_id');
    }
    
    
}
