<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 

class Game extends BaseModel
{ 
    use UID;
    
    protected $table = 'game';
    protected $keyType = 'string';
    protected $fillable = array('*');
    
    public const types =  [
        ['id'=>1,'name' => '戳戳樂'],  
        ['id'=>2,'name' => '拉霸'],
    ];
    
    public static function typeArray() {
        $arr  = [];
        foreach (self::types as $type){
            $arr[$type["id"]] =   $type["name"];
        }
        return $arr;
    }
    
    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }
    
    /**
     * 取得獎品設定
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bonus() {
        return $this->hasMany(Bonus::class,   "game_id");
    }
  
}
