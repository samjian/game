<?php
namespace App\Models;



class LotteryList extends BaseModel
{
    protected $fillable = array('*');
    
    
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    
    public function game()
    {
        return $this->belongsTo(Game::class,'game_id');
    }
    
    public function bonus()
    {
        return $this->belongsTo(Bonus::class,'bonus_id');
    }
    
    public function store()
    {
        return $this->belongsTo(Store::class,'store_id');
    }
}