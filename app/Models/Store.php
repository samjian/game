<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends BaseModel
{
    use UID;
    
    protected $fillable = array('*');
    protected $keyType = 'string';
    
    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }
    
}
