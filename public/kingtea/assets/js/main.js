/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src@4.0/assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src@4.0/assets/js/main.js":
/*!***********************************!*\
  !*** ./src@4.0/assets/js/main.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// ajax 配合JQ3 引入設置
$(document).ready(function () {
  $.ajax({
    url: "/game/public/kingtea/ajax/_header.html",
    method: "GET",
    dataType: "html"
  }).done(function (data) {
    $("#header").html(data);
    var opennav = document.querySelector(".l-openbtn");
    var closenav = document.querySelector(".l-closebtn");

    function toggleMask() {
      document.getElementById("mask").classList.toggle("mask-open");
    }

    function openNav() {
      document.getElementById("mySidenav_sc").classList.add("l-sidenav-open"); // console.dir(document.getElementById("mask"))
    }

    function closeNav() {
      document.getElementById("mySidenav_sc").classList.remove("l-sidenav-open");
    }

    opennav.addEventListener("click", function () {
      toggleMask();
      openNav();
    });
    closenav.addEventListener("click", function () {
      toggleMask();
      closeNav();
    });
  });
  $("#footer").load("/game/public/kingtea/ajax/_footer.html");
});

function toolsListener() {
  window.addEventListener("keydown", function (e) {
    if (e.keyCode === 9) {
      document.body.classList.remove("js-useMouse");
      document.body.classList.add("js-useKeyboard");
    }
  });
  window.addEventListener("mousedown", function (e) {
    document.body.classList.remove("js-useKeyboard");
    document.body.classList.add("js-useMouse");
  });
} // prevent orientation
//呼叫function-網頁載入完成後


$(document).ready(function () {
  toolsListener();
}); //呼叫function-視窗大小變更

$(window).resize(function () {}); //呼叫function-捲動

$(window).scroll(function () {});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjQDQuMC9hc3NldHMvanMvbWFpbi5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImFqYXgiLCJ1cmwiLCJtZXRob2QiLCJkYXRhVHlwZSIsImRvbmUiLCJkYXRhIiwiaHRtbCIsIm9wZW5uYXYiLCJxdWVyeVNlbGVjdG9yIiwiY2xvc2VuYXYiLCJ0b2dnbGVNYXNrIiwiZ2V0RWxlbWVudEJ5SWQiLCJjbGFzc0xpc3QiLCJ0b2dnbGUiLCJvcGVuTmF2IiwiYWRkIiwiY2xvc2VOYXYiLCJyZW1vdmUiLCJhZGRFdmVudExpc3RlbmVyIiwibG9hZCIsInRvb2xzTGlzdGVuZXIiLCJ3aW5kb3ciLCJlIiwia2V5Q29kZSIsImJvZHkiLCJyZXNpemUiLCJzY3JvbGwiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBQSxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVk7QUFDNUJGLEdBQUMsQ0FBQ0csSUFBRixDQUFPO0FBQ0xDLE9BQUcsRUFBRSxtQkFEQTtBQUVMQyxVQUFNLEVBQUUsS0FGSDtBQUdMQyxZQUFRLEVBQUU7QUFITCxHQUFQLEVBSUdDLElBSkgsQ0FLRSxVQUFVQyxJQUFWLEVBQWdCO0FBQ2RSLEtBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYVMsSUFBYixDQUFrQkQsSUFBbEI7QUFDQSxRQUFJRSxPQUFPLEdBQUdULFFBQVEsQ0FBQ1UsYUFBVCxDQUF1QixZQUF2QixDQUFkO0FBQ0EsUUFBSUMsUUFBUSxHQUFHWCxRQUFRLENBQUNVLGFBQVQsQ0FBdUIsYUFBdkIsQ0FBZjs7QUFFQSxhQUFTRSxVQUFULEdBQXNCO0FBQ3BCWixjQUFRLENBQUNhLGNBQVQsQ0FBd0IsTUFBeEIsRUFBZ0NDLFNBQWhDLENBQTBDQyxNQUExQyxDQUFpRCxXQUFqRDtBQUNEOztBQUVELGFBQVNDLE9BQVQsR0FBbUI7QUFDakJoQixjQUFRLENBQUNhLGNBQVQsQ0FBd0IsY0FBeEIsRUFBd0NDLFNBQXhDLENBQWtERyxHQUFsRCxDQUFzRCxnQkFBdEQsRUFEaUIsQ0FFakI7QUFDRDs7QUFFRCxhQUFTQyxRQUFULEdBQW9CO0FBQ2xCbEIsY0FBUSxDQUFDYSxjQUFULENBQXdCLGNBQXhCLEVBQXdDQyxTQUF4QyxDQUFrREssTUFBbEQsQ0FBeUQsZ0JBQXpEO0FBQ0Q7O0FBQ0RWLFdBQU8sQ0FBQ1csZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUM1Q1IsZ0JBQVU7QUFDVkksYUFBTztBQUNSLEtBSEQ7QUFJQUwsWUFBUSxDQUFDUyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxZQUFZO0FBQzdDUixnQkFBVTtBQUNWTSxjQUFRO0FBQ1QsS0FIRDtBQUlELEdBOUJIO0FBZ0NBbkIsR0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFhc0IsSUFBYixDQUFrQixtQkFBbEI7QUFDRCxDQWxDRDs7QUFvQ0EsU0FBU0MsYUFBVCxHQUF5QjtBQUN2QkMsUUFBTSxDQUFDSCxnQkFBUCxDQUF3QixTQUF4QixFQUFtQyxVQUFVSSxDQUFWLEVBQWE7QUFDOUMsUUFBSUEsQ0FBQyxDQUFDQyxPQUFGLEtBQWMsQ0FBbEIsRUFBcUI7QUFDbkJ6QixjQUFRLENBQUMwQixJQUFULENBQWNaLFNBQWQsQ0FBd0JLLE1BQXhCLENBQStCLGFBQS9CO0FBQ0FuQixjQUFRLENBQUMwQixJQUFULENBQWNaLFNBQWQsQ0FBd0JHLEdBQXhCLENBQTRCLGdCQUE1QjtBQUNEO0FBQ0YsR0FMRDtBQU1BTSxRQUFNLENBQUNILGdCQUFQLENBQXdCLFdBQXhCLEVBQXFDLFVBQVVJLENBQVYsRUFBYTtBQUNoRHhCLFlBQVEsQ0FBQzBCLElBQVQsQ0FBY1osU0FBZCxDQUF3QkssTUFBeEIsQ0FBK0IsZ0JBQS9CO0FBQ0FuQixZQUFRLENBQUMwQixJQUFULENBQWNaLFNBQWQsQ0FBd0JHLEdBQXhCLENBQTRCLGFBQTVCO0FBQ0QsR0FIRDtBQUlELEMsQ0FDRDtBQUdBOzs7QUFDQWxCLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsWUFBWTtBQUM1QnFCLGVBQWE7QUFDZCxDQUZELEUsQ0FHQTs7QUFDQXZCLENBQUMsQ0FBQ3dCLE1BQUQsQ0FBRCxDQUFVSSxNQUFWLENBQWlCLFlBQVksQ0FBRSxDQUEvQixFLENBQ0E7O0FBQ0E1QixDQUFDLENBQUN3QixNQUFELENBQUQsQ0FBVUssTUFBVixDQUFpQixZQUFZLENBQUUsQ0FBL0IsRSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmNANC4wL2Fzc2V0cy9qcy9tYWluLmpzXCIpO1xuIiwiLy8gYWpheCDphY3lkIhKUTMg5byV5YWl6Kit572uXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gICQuYWpheCh7XG4gICAgdXJsOiBcImFqYXgvX2hlYWRlci5odG1sXCIsXG4gICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgIGRhdGFUeXBlOiBcImh0bWxcIlxuICB9KS5kb25lKFxuICAgIGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAkKFwiI2hlYWRlclwiKS5odG1sKGRhdGEpO1xuICAgICAgdmFyIG9wZW5uYXYgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmwtb3BlbmJ0blwiKTtcbiAgICAgIHZhciBjbG9zZW5hdiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubC1jbG9zZWJ0blwiKTtcblxuICAgICAgZnVuY3Rpb24gdG9nZ2xlTWFzaygpIHtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYXNrXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJtYXNrLW9wZW5cIik7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIG9wZW5OYXYoKSB7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibXlTaWRlbmF2X3NjXCIpLmNsYXNzTGlzdC5hZGQoXCJsLXNpZGVuYXYtb3BlblwiKTtcbiAgICAgICAgLy8gY29uc29sZS5kaXIoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYXNrXCIpKVxuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBjbG9zZU5hdigpIHtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJteVNpZGVuYXZfc2NcIikuY2xhc3NMaXN0LnJlbW92ZShcImwtc2lkZW5hdi1vcGVuXCIpO1xuICAgICAgfVxuICAgICAgb3Blbm5hdi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICB0b2dnbGVNYXNrKCk7XG4gICAgICAgIG9wZW5OYXYoKTtcbiAgICAgIH0pO1xuICAgICAgY2xvc2VuYXYuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdG9nZ2xlTWFzaygpO1xuICAgICAgICBjbG9zZU5hdigpO1xuICAgICAgfSk7XG4gICAgfVxuICApO1xuICAkKFwiI2Zvb3RlclwiKS5sb2FkKFwiYWpheC9fZm9vdGVyLmh0bWxcIik7XG59KTtcblxuZnVuY3Rpb24gdG9vbHNMaXN0ZW5lcigpIHtcbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgaWYgKGUua2V5Q29kZSA9PT0gOSkge1xuICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKFwianMtdXNlTW91c2VcIik7XG4gICAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5hZGQoXCJqcy11c2VLZXlib2FyZFwiKTtcbiAgICB9XG4gIH0pO1xuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCBmdW5jdGlvbiAoZSkge1xuICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LnJlbW92ZShcImpzLXVzZUtleWJvYXJkXCIpO1xuICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmFkZChcImpzLXVzZU1vdXNlXCIpO1xuICB9KTtcbn1cbi8vIHByZXZlbnQgb3JpZW50YXRpb25cblxuXG4vL+WRvOWPq2Z1bmN0aW9uLee2sumggei8ieWFpeWujOaIkOW+jFxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICB0b29sc0xpc3RlbmVyKCk7XG59KTtcbi8v5ZG85Y+rZnVuY3Rpb24t6KaW56qX5aSn5bCP6K6K5pu0XG4kKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uICgpIHt9KTtcbi8v5ZG85Y+rZnVuY3Rpb24t5o2y5YuVXG4kKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uICgpIHt9KTsiXSwic291cmNlUm9vdCI6IiJ9