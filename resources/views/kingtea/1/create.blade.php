@extends('kingtea.1.layout.main')

@section('content')
<script type="text/javascript">
	
 $(function() {

	 $.validator.methods.matches = function (value, element, params) {
	    var re = new RegExp(params);
	    return this.optional(element) || re.test(value);
	 };
	  
	 $("#form").validate({
		  rules:  {
			 name: "required",
			 phone : {
				 required:true,
			     matches:  "^09\\d{8}$",
		     },
		     
		     email: {
		        required: true,
		        email: true
		     },	
 
	      }, 
	      messages: {
	    	 name: "請輸入姓名",
	    	 email:"請輸入正確的Email",
	    	 phone:"您的手機號碼格式不正確",
		  },
		 
		  submitHandler:function(form){
			  if(!$("#agree").prop("checked")) {
				    alert("請同意服務條款!");
				    return false;
			  }

			 $("#form").submit();			   
          }
	 });

	 
 });
</script>
 

    <!-- ============= 主要內容區 ============= -->
 <main class="h-100">
      <section class="h-100">
        <div class="p-line text-center py-4">
          <h4 class="mb-0">好友LINE一下 戳戳好運</h4>
        </div>
        <div class="p-content">
          <div class="content">
            <p class="pt-4 m-0">請務必留下真實姓名及連絡電話，以供中獎資格確認及獎品寄出作業！</p>
           <form id="form" name="form" action="{{url('play/create')}}" method = "post">
              @csrf
              <input  type="hidden" name="store_id" value="{{$store_id}}"/>
              <input  type="hidden" name="game_id" value="{{$game_id}}"/>
              <input  type="hidden" name="line_id" value="{{$line_id}}"/>
              <input type="text" id="name" name="name" class="l-form-field mt-4" placeholder="姓名" />
              <input type="text" id="phone" name="phone" class="l-form-field mt-3" placeholder="手機號碼" />
              <input type="email" id="email" name="email"  class="l-form-field mt-3" placeholder="E-mail" />
              <!-- 
              <input type="checkbox" id="agree" name="agree" class="l-form-check-input">
               -->
              <input type="checkbox" class="l-form-check-input" id="agree" name="agree">
              <label for="agree" class="l-form-check-label">
             	   當您使用本服務，您已表示同意服務條款、抽獎活動規則及隱私權政策。
              </label>
              <button type="submit" id="btnSend" class="p-submit"  >確認送出</button>
            </form>
          </div>
        </div>
  </section>
 </main>
 
 @endsection  