@extends('kingtea.1.layout.main')

@section('content')

<script type="text/javascript">
     var lottery_num =  "{{$user->lottery_num}}";

     var isSend = false;
     
     function again() {

    	 if (lottery_num >3) {
		    //alert("遊戲一天只能玩三次!");
		    $("#btn").html("明日加油");
		    $("#btn").attr("href", "javascript:viod(0)");

			if (isSend) {
    		    $.get("{{url('/play/sendGameResult')}}", function(result){
    				console.log(result);
    			});
    
    		    isSend = true;
			}

		    return;
         }  
   
         window.location = "{{url('play/toGame')}}/{{$store_id}}/{{$game_id}}";
     }


     
     setTimeout(function() {
    	 if (isSend) {
  		    $.get("{{url('/play/sendGameResult')}}", function(result){
  				console.log(result);
  			});
  
  		    isSend = true;
 		 }
     }, 1000 * 60);
     
</script>


    <!-- ============= 主要內容區 ============= -->
    <main class="h-100">
      <section class="h-100">
        <div class="p-line text-center py-4">
          <h4 class="mb-0">好友LINE一下 戳戳好運</h4>
        </div>
        <div class="p-content text-center">
          <div class="content">
            <h4 class="pt-4 m-0">未中獎，再接再厲！</h4>
            <div class="u-mt-500">
              <img class="p-cap-bye" src="/game/public/kingtea/assets/img/index/cap_bye.svg">
            </div>
            
            <a id="btn" class="p-submit" href="javascript:again()">再玩一次</a>
          </div>
        </div>
  </section>
  </main>
  <!-- =============end 主要內容區 ============= -->
@endsection 