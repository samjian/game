<!DOCTYPE html>
<html lang="zh-tw">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>茶裏王</title>
  <meta name="keyword" content="關鍵字" />
  <meta name="description" content="好友LINE一下 戳戳好運" />
  <meta name="author" content="作者" />
  <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8" />
  

  <link rel="shortcut icon" href="/game/public/kingtea/favicon.ico" />
  <link rel="stylesheet" href="/game/public/kingtea/assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/game/public/kingtea/assets/plugins/fontawesome@5/css/all.min.css" />
  <link rel="stylesheet" href="/game/public/kingtea/assets/css/main.css" />
  
  
   <!-- JS Global Compulsory -->
  <script src="/game/public/kingtea/assets/plugins/jquery@3.5.1.min.js"></script>
  <script src="/game/public/kingtea/assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
  <script src="/game/public/kingtea/assets/js/main.js"></script>
  <!-- JS Customization -->
  
  <script src="https://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
  <script src="https://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  
  <style type="text/css">
    .error {
      color: #F00;
    } 
  </style>
  <script type="text/javascript">
 
	 window.alert = function(msg) {
 
		 swal({
          title: "提示!",
          text: msg,
          icon: "warning",
       
        })
     }
 
     @if (session('msg'))
 	  $(function(){
 		   alert("{{ session('msg') }}");
           {{session()->forget('msg') }}
 	  });
     @endif;
  </script>
</head>

<body>
  <noscript>
    <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
      您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
    </span>
  </noscript>
  <div id="page" class="h-100">
    <div id="mask" class="mask"></div>
    <!-- ============= 導覽列 ============= -->
    <header id="header" class="l-header"></header>
    <!-- =============end 導覽列 ============= -->
     
     <!-- 子內容 個別顯示 START-->
     @yield('content')
     <!-- 子內容 個別顯示 END-->
     
  <!-- =============end 主要內容區 ============= -->
  <!-- ============= footer ============= -->
  <footer id="footer" class="l-footer"></footer>
  <!-- =============end footer ============= -->
  </div>
 

</body>

</html>