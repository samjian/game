@extends('kingtea.1.layout.main')

@section('content')
    <!-- ============= 主要內容區 ============= -->
  
    <main class="h-100">
      <section class="h-100">
        <div class="p-line text-center py-4">
          <h4 class="mb-0">好友LINE一下 戳戳好運</h4>
        </div>
        <div class="p-content">
         
          <div class="content">
            <p class="pt-4 m-0 text-center font-weight-bold">請點選任一瓶蓋，即有機會戳中大獎！</p>
            <div class="container justify-content-between">
               <form id="form" action="{{url('play/lottery')}}/{{$store_id}}/{{$game_id}}" method="post">
                   @csrf
                   <div class="mt-4 row">
                        <a class="col-4 p-1" href="javascript:form.submit()">
                          <img class="w-100" src="/game/public/kingtea/assets/img/index/cap.svg">
                        </a>
                        <a class="col-4 p-1" href="javascript:form.submit()">
                          <img class="w-100" src="/game/public/kingtea/assets/img/index/cap.svg">
                        </a>
                        <a class="col-4 p-1" href="javascript:form.submit()">
                          <img class="w-100" src="/game/public/kingtea/assets/img/index/cap.svg">
                        </a>
                        <a class="col-4 p-1" href="javascript:form.submit()">
                          <img class="w-100" src="/game/public/kingtea/assets/img/index/cap.svg">
                        </a>
                        <a class="col-4 p-1" href="javascript:form.submit()">
                          <img class="w-100" src="/game/public/kingtea/assets/img/index/cap.svg">
                        </a>
                        <a class="col-4 p-1" href="javascript:form.submit()">
                          <img class="w-100" src="/game/public/kingtea/assets/img/index/cap.svg">
                        </a>
                        <a class="col-4 p-1" href="javascript:form.submit()">
                          <img class="w-100" src="/game/public/kingtea/assets/img/index/cap.svg">
                        </a>
                        <a class="col-4 p-1" href="javascript:form.submit()">
                          <img class="w-100" src="/game/public/kingtea/assets/img/index/cap.svg">
                        </a>
                        <a class="col-4 p-1" href="javascript:form.submit()">
                          <img class="w-100" src="/game/public/kingtea/assets/img/index/cap.svg">
                        </a>
                  </div>
              </form>
            </div>
          </div>
        </div>
  </div>

  </section>
  </main>
 
  <!-- =============end 主要內容區 ============= -->
 @endsection  