@extends('kingtea.1.layout.main')

@section('content')

    <!-- ============= 主要內容區 ============= -->
    <main class="h-100">
      <section class="h-100">
        <div class="p-line text-center py-4">
          <h4 class="mb-0">好友LINE一下 戳戳好運</h4>
        </div>
        <div class="p-content text-center">
          <div class="content">
            <h4 class="pt-4 m-0">恭喜中獎</h4>
            <div class="d-flex justify-content-center">
              <div class="p-cap-blank">
                <img class="p-iphone" src="{{env('APP_URL').'/public/uploads/'.$bonus->img}}">
              </div>
            </div>
            <a class="p-submit" href="">立即兌換</a>
          </div>
        </div>

  </div>

  </section>
  </main>
  

@endsection 