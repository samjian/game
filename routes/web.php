<?php
use App\Http\Controllers\PlayController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/play/{store_id}/{game_id}',  "PlayController@index");


Route::group([ 'middleware' => ["playMiddleware"]], function(){
    
    //首頁
    Route::get('/play/login/{store_id}/{game_id}',  "PlayController@login");
    
    //前往新增
    Route::get('/play/toCreate/{store_id}/{game_id}}',  "PlayController@toCreate");
    
    //前往遊戲
    Route::get('/play/toGame/{store_id}/{game_id}',  "PlayController@toGame");
    
    //抽獎
    Route::post('/play/lottery/{store_id}/{game_id}',  "PlayController@lottery");
    
    //新增資料
    Route::post('/play/create',  "PlayController@create");
     
    //傳送結果
    Route::get('/play/sendGameResult', "PlayController@sendGameResult");
});


//Line 登入
Route::get('/line', "PlayController@line");

/** 以下為測試*/

Route::get("/test", "TestController@test");


Route::get('/export/lottery/{gameId}', 'ExcelController@lottery');

 

 